from django.conf.urls import patterns, include, url
from django.contrib import admin
from . import push_notification

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'betterworks.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^betterworks/', include('betterworks.urls')),
    url(r'^$',push_notification.index, name='index'),
    url(r'^trigger_notification$', push_notification.trigger_notification, name='trigger_notification'),
    url(r'^enter_notification$', push_notification.enter_notification, name='enter_notification'),
    url(r'^login$', push_notification.login, name='login'),
    url(r'^admin/', include(admin.site.urls)),
)
