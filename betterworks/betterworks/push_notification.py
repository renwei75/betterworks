from django.http import HttpResponse
from pusher import Pusher


def index(request, login_failed):
    heading = 'Login failed! <br>Please reenter user_id, password' if login_failed else 'Please enter user_id, password'
    response = """
<head></head><body>%s<form action="login" method="POST">
User Id:<br>
<input type="text" name="user_id">
<br>
Password:<br>
<input type="password" name="password">
<input type="hidden" name="channel_name" value='test_channel'>
<input type="hidden" name="socket_id" value='12345.6789'>
<br><br>
<input type="submit" value="login">
</form>
</body>
    """ % heading
    return HttpResponse(response)


def login(request):
    try:
        user_id = request.POST['user_id']
        password = request.POST['password']
        channel = request.POST['channel_name']
        socket_id = request.POST['socket_id']
        # a user auth against LDAP or Active Directory should be added here
        if not (user_id == '123' and password == '123'):
            raise KeyError
    except KeyError:
        login_failed = True if ('user_id' in request.POST and 'password' in request.POST) else False
        return index(request, login_failed)
    else:
        # all the values related to the pusher should be in a config file or database
        pusher = Pusher(
            app_id=u'125169',
            key=u'efb7486cf469319b3531',
            secret=u'6bf687ee86e5e8612591'
        )
        auth = pusher.authenticate(
            channel,
            socket_id
        )
        return start_web_socket(request)


def start_web_socket(request):
    # pusher key, channel name and event name should be dynamically populated to the javascript
    response = """<!DOCTYPE html>
<head>
  <title>Pusher Test</title>
  <script src="//js.pusher.com/2.2/pusher.min.js"></script>
  <script>
    // Enable pusher logging - don't include this in production
    Pusher.log = function(message) {
      if (window.console && window.console.log) {
        window.console.log(message);
      }
    };

    var pusher = new Pusher('efb7486cf469319b3531');
    var channel = pusher.subscribe('test_channel');
    channel.bind('my_event', function(data) {
      alert(data.message);
    });
  </script>
</head>
<body>
<h3>This is the client to receive the notification.<br><br>
Open <a href="enter_notification">enter_notification</a> page in different tab or browser to start sending notifications.</h3></body>"""
    return HttpResponse(response)


def enter_notification(request):
    response = """
    <head></head><body>Please fill the info and click submit<form action="trigger_notification" method="POST">
User Id:<br>
<input type="text" name="user_id">
<br>
Message:<br>
<input type="text" name="message">
<br><br>
<input type="submit" value="submit">
</form>
</body>"""
    return HttpResponse(response)


def trigger_notification(request):
    try:
        user_id = request.POST['user_id']
        message = request.POST['message']
        push_to_user(user_id, message)
        response = "You triggered notification to user [%s], notification [%s]."
    except (KeyError):
        response = "Error: You didn't provide user_id or message to trigger notification.",
        return HttpResponse(response)
    else:
        return HttpResponse(response % (user_id, message))


def push_to_user(user_id, message):
    pusher = Pusher(
        app_id=u'125169',
        key=u'efb7486cf469319b3531',
        secret=u'6bf687ee86e5e8612591'
    )

    pusher.trigger(u'test_channel', u'my_event', {u'user_id': user_id, u'message': message})
    return